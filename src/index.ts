// include dependencies
import {createProxyMiddleware} from 'http-proxy-middleware'
import express from 'express';

const app = express();

const port = process.env.PORT ?? 3000;
const target = process.env.TARGET || 'http://www.example.org';

console.table({port, target});

app.use(createProxyMiddleware({
    target: target,
    changeOrigin: true, // needed for virtual hosted sites
    plugins: [
        (proxyServer, _) => {
            // On Request time we need to remember when the request was received
            proxyServer.on('proxyReq', (_, req) => {
                // console.log(`Got request for '${req.url}', rewriting to ${target}`);

                // @ts-ignore
                req.requestReceivedT = Date.now();
            })

            // On Response time we know how long it took by calculating the difference between the timestamp saved on proxyReq and now
            // We can then generate the log entry and spit it out to the console
            // Format: [<requestReceivedT>]|[<responseT>]|<duration>|<responseBodySize>
            proxyServer.on('proxyRes', (proxyRes, req) => {
                // @ts-ignore
                const requestReceivedT = req.requestReceivedT || 0;
                const responseT = Date.now();
                const duration = responseT - requestReceivedT;
                const responseBodySize = parseInt(proxyRes.headers['content-length'] || '0', 10);

                console.log(`[${requestReceivedT}]|[${responseT}]|[${duration}]|[${responseBodySize}]`);
            })
        }
    ]
}));


app.listen(port, () => {
    console.log("Server is running on port " + port);
});

process.on('SIGINT', function() {
    console.log("Caught interrupt signal, exiting...");
    process.exit();
});

process.on('SIGTERM', function() {
    console.log("Caught interrupt signal, exiting...");
    process.exit();
});

