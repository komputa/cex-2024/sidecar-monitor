FROM node as builder

COPY src src
COPY package.json package.json
COPY tsconfig.json tsconfig.json
COPY package-lock.json package-lock.json

RUN npm install

RUN npm run build

FROM node as proddeps

COPY package.json package.json
COPY package-lock.json package-lock.json

RUN npm install --production

FROM node:alpine as final

WORKDIR /app

COPY --from=builder ./dist ./

COPY --from=proddeps node_modules node_modules

ENV NODE_ENV production

EXPOSE 3000

CMD ["node", "index.js"]
